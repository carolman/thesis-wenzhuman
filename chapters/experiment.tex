\chapter{Experimental Results} \label{experiment}
We used our tool to analyze several popular, deployed, font files belonging to the Google Noto Fonts suite\footnote{\url{https://www.google.com/get/noto/}}, thus demonstrating the effectiveness of our work. Our experimental subjects are the NotoKufiArabic, NotoSansBoldItalic,
NotoSansBold, and NotoSansItalic fonts. The sizes for 
NotoKufiArabic, NotoSansBoldItalic,
NotoSansBold, and NotoSansItalic are 1.2 MB, 7.6 MB, 7.5 MB, and 7.8 MB separately.

Our tool successfully transformed all the glyph programs, control value programs and font programs of these 
font files into COI. We also performed constant 
propagation on the generated COI to compute the set of functions which are ever called from control value programs and from every glyph program.

Table~\ref{table:callsiteresult} summarizes the experimental results, presenting statistics about the call graph, particularly the number of reachable functions from the control value program and from the glyph programs. Table \ref{table:callsiteresult2} shows the actual function call sites in each glyph in the font NotoKufiArabic. Functions that are (transitively) called in the control value program can be reached throughout the font file (labelled global in the table), since the control value program is executed before every glyph in the font. Functions that are called from glyph programs can only be reached locally (labeled local in the table). We have cross-checked these remarkable results by instrumenting the FreeType interpreter to report counts of reachable functions.
\begin{table}[h]
\caption{Counts of reachable functions in selected Google Noto Sans fonts. Under 15\% of bytecode functions are reachable.} 
\label{table:callsiteresult}
\centering
\vspace*{1em}

\begin{tabular}{lrrrrrr}
  Font Name & \# Glyphs & \# Functions & \# Functions
 & \# Functions 
&Functions \\
&&&reachable & reachable & reachable\\
&&&globally & locally&in total\\
\toprule
  KufiArabic & 392 & 91 & 4 & 2 &6\\
  \toprule
  SansBoldItalic & 2101 & 91 & 5&5&10 \\
  \toprule
  SansBold& 2330 & 91 & 5&5&10\\
  \toprule
  SansItalic &2111 & 91 & 5&6&11\\
  \toprule
\end{tabular}
\end{table}

As shown in Table \ref{table:callsiteresult}, NotoKufiArabic defines outlines for 392 glyphs and includes 90 functions in its font program. However, the control value program only calls 4 functions. Also, only 2 functions are called from any glyph programs for NotoKufiArabic. 
Table~\ref{table:callsiteresult} further shows that only a very small fraction (from 6/91 to 11/91) of the functions in the function table are actually used in the other Google Noto fonts. 

To better understand the use of functions in font files, we also calculate the number of static callsites to each function in our four subject fonts.
Table~\ref{table:callsitecounts} shows another view of the reachable functions information from Table~\ref{table:callsiteresult}. Glyph programs in every font tend to call certain functions many times. In NotoKufiArabic, there are 258 call sites which invoke function \#89. In NotoSansBoldItalic, the numbers of call sites of function \#38, \#72 and \#73 are 598, 222, and 596.

\begin{table}[h]
\caption{Counts of static callers to functions are concentrated for the KufiArabic, SansBoldItalic, SansBold, and SansItalic fonts.}
\label{table:callsitecounts}
\vspace*{1em}
\centering
\begin{tabular}{lrrrr}
  function  & \# callers & \# callers & \# callers & \# callers \\
  label &KufiArabic & SansBoldItalic & SansBold & SansItalic \\
\toprule
14         &    &    1&   &\\
31 (global)&  1 &   11& 14&13\\
37         &    &     &930&\\
38         &    &  595&224&593\\
62         &    &   89&   &94\\
70 (global)&    &   9&   3&2\\
72         &  12& 222& 202&159\\
73         &    & 596&  59&250\\
83 (global)&   1&   1&   1&1\\
84 (global)&   1&   1&   1&1\\
85 (global)&   2&  35&  31&35\\
89         & 258&1476&1432&1482 \\
91         &    &   4&    &4
\end{tabular}
\end{table}

We speculate that the large amounts of unused code might result from automatic hinting tools:
producing a high-quality hint program requires both artistry and programming knowledge. Font designers
appear to often use existing libraries of hint programs.

Appendix \ref{Appendixb} presents our experimental results in more detail. It shows the exact function label invoked at each call site of the Control Value Program and in glyph programs. Given this information, font subsetting tools will be able to determine which functions are used and which functions are not. This information permits the removal of unused functions directly from the font files by existing font manipulation tools.

Our experimental results provide helpful information
usable by font subsetting tools in their efforts to remove unused
instructions. We have also found that implementing the constant
propagation was aided by the use of COI. The three-address code property of COI
enables optimization for TrueType bytecode.

