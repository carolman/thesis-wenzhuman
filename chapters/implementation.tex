\chapter{Design and Implementation}
Figure \ref{fig:Overview} shows the steps we take for TrueType optimization and how all of our components fit together. First, we transform TrueType bytecode into COI, using results from the abstract interpreter. Second, we optimize the COI bytecode. Finally, (although this step is not implemented yet), we would transform COI back into TrueType.

\begin{figure}[h!]
\centering
\hspace{1cm}
\tikzstyle{block} = [rectangle, draw, 
text width=6.5em, text centered, rounded corners, minimum height=4em]
\tikzstyle{line} = [draw, -latex']
\tikzstyle{cloud} = [draw, ellipse, node distance=3cm,
minimum height=4em]
\begin{tikzpicture}[node distance = 3cm, auto]
\node [block] (tb) {TrueType byte code};
\node [cloud, right of=tb, node distance=5.5cm] (tr) {Transformation};
\node [block, right of=tr, node distance=5.5cm] (coi) {COI (TAC) code};
\node [block, above of=tr, node distance=3.5cm] (at) {Abstract Interpreter};
\node [block, below of=coi] (ocoi) {Optimized COI (TAC) code};
\node [block, dashed, below of=ocoi] (otb) {Optimized TrueType byte code};
\path [line] (tb) -- (tr);
\path [line] (tr) -- (coi);
\path [line] (at) -- (tr);
\path [line] (coi) -- node{Optimization} (ocoi);
\path [line,dashed] (ocoi) -- (otb);
\end{tikzpicture} 
\caption{\label{fig:Overview}Relationships between COI components. Dashed box not yet implemented.}
\end{figure}
\section{Abstract Interpreter}
\subsection{Observations}
Our design was influenced by two observations about TrueType bytecode, as mentioned in the previous chapter:
\begin{itemize}
\item Stack depth: due to the presence of loop instructions and implicitly-targetted function calls, calculating the current stack depth for every instruction is challenging.
\item Data types: Although there are no explicit stack data types in TrueType syntax, TrueType instructions do use different types for each operand: integer, shortFrac, Fixed, FWord, uFWord, F2Ddot14 and longDateTime. 
\end{itemize}
To analyze the behaviour of instructions in loops, we need the value of the loop state variable.
Moreover, we need to accurately associate instructions with their operands, which are on the program stack.
To gain all this information, we might consider implementing a TrueType interpreter.
However, unlike a real font interpreter, at analysis time, we do not have run-time context, such as the target PPEM and the version number of the font scaler (engine).
Moreover, we don't need all the data in program stack to be concrete---unlike a true font engine, we will not rasterize the font nor will we generate bitmap images for glyphs.
       
Futhermore, implementing a full TrueType interpreter requires a lot of work and it provides more information than we need for stack-based bytecode transformation. 
For example, we don't need the information about the zp0, zp1, zp2 variables in the graphics state, as they are only used in generating glyph images.

Our second observation provides guidance for differentiating unnecessary information from necessary information on the program stack. We only need to store and calculate concrete values for integer-typed data. We can use abstract values to represent other graphics-related data on the program stack.

We therefore use an abstract interpreter as a first stage in our analysis framework. 
Our interpreter introduces abstract values to represent data on the program stack.

For data which is either unknown (because it is program input) or associated with points and vectors, we create a set of abstract dataType classes, which we can put onto the stack in the place of normal values.

Consider, for instance, our interpretation of the \textbf{MPPEM} instruction. Since the PPEM is unknown, we
push an object of the abstract data type class \textbf{PPEM\_Y()} or \textbf{PPEM\_X()}, representing the result, onto the program stack.
\begin{lstlisting}
	def exec_MPPEM(self):
        if self.graphics_state['pv'] == (0, 1):
            self.program_stack.append(dataType.PPEM_Y())
        else:
            self.program_stack.append(dataType.PPEM_X())
\end{lstlisting}

For concrete integer-typed values, we calculate a concrete result and push that result back onto the program stack.

If the operands of an expression contain both abstract values and concrete integer-typed values, we use an expression containing both the abstract value and the concrete value to represent the result, and push that result back onto the stack. Hence, data on our stack are expressions potentially containing both concrete values and abstract values.

In COI, all data on the stack get an associated variable name, or identifier. Then, in COI instructions, we use explicit variable identifiers instead of implicit stack indices to refer
to this data. In this way, every operand of every COI instruction is explicit.
\subsection{Type Inference} 
The TrueType manual provides type information for instructions, namely for their inputs (data pushed onto the stack) and outputs (data popped from the stack). Our abstract interpreter uses this information. 
In this section, we give an example and discuss how we infer type information for data on the stack.

Consider instruction \textbf{ROUND}. In the TrueType specification, the round algorithm is quite complex. However, the actual values are unnecessary for our purposes. In our \textbf{exec\_ROUND} implementation, we simply use a F26Dot6 object to represent uninterpreted rounded values on the stack, as shown in Figure~\ref{fig:f26}. 
\begin{figure}[h]
  \centering
\begin{lstlisting}
	def exec_ROUND(self):
        self.program_stack_pop()
        self.program_stack.append(dataType.F26Dot6())
\end{lstlisting}
\caption{\label{fig:f26}Implementation of ROUND in abstract interpreter}

\end{figure}
\subsection{Accurate Stack Height}
Even though there are abstract values on the stack, the stack height is always accurate.
This is because the number of popped values and pushed values is specified for most instructions.
The exception is loop-related instructions. Fortunately, in practice, we found that their effects on the stack height could also be accurately calculated by our abstract interpreter. Loops rely on the loop variable in graphics state, which we were able to always keep concrete.

\subsection{Implementation}
Our interpreter implementation contains two main steps. The first step is to tokenize plain strings in the TTX input into instructions and then construct a list of instruction instances.
The second step is to structure the TrueType instruction list and build a successor list for every instruction.

For example, in Figure \ref{fig:f-if}, the successors for the IF on line 3 are 1) the DUP on line 4 and 2) the POP on line 17.
Similarly, the successors for the IF on line 10 are 1) the WCVTP on line 11 and 2) the POP on line 13. 
\begin{figure}[h]
\caption{TrueType bytecode code which includes IF-ELSE instructions}
\label{fig:f-if}
  \centering
\begin{lstlisting}
   MPPEM([])
   LTEQ([])
   IF([])
      DUP([])
      PUSH([3])
      CINDEX([])
      RCVT([])
      ROUND([1])
      GTEQ([])
     IF([])
        WCVTP([])
     ELSE([])
        POP([])
        POP([])
     EIF([])
  ELSE([])
     POP([])
     POP([])
  EIF([])
\end{lstlisting}
\end{figure} 
\subsubsection{ExecutionContext}
We designed a class called \textbf{ExecutionContext} to represent the execution environment for every TrueType bytecode, including a program stack, a CVT table, a storage area, and a representation of the current graphics state. 

We implemented a method exec\_InstructionName
for every TrueType instruction. This method
modifies the current ExecutionContext with the effects of InstructionName.


\subsubsection{Use stack to store back pointers}
Since a program is a sequence of TrueType instructions, we process instructions one-by-one from the instruction stream. Because a normal interpreter only processes concrete values, it only executes one successor per instruction, depending on the value.
This approach works because, in most cases, instructions have only one successor. 

However, things get more complicated when we encounter function calls, since calls modify the flow of control. 
In addition, some instructions have more than one successor. 
As we may have abstract values on our stack, there may not be enough information for us to determine which successor should be executed. As a result, we sometimes need to traverse all possible branches.

We therefore implement a special method to simulate the program counter correctly.

Furthermore, when we are traversing nested if-else blocks, and execute a chain of function calls, we need to store more than one program counter.
To track all program counter values, we use a stack to store back pointers, leveraging
the stack last-in-first-out property. This enables us to correctly return to the last back pointer stored upon block end or return.

In the following, we will explain our treatment of function calls and if-else clauses.
\begin{itemize}
\item \textbf{Function Call} As discussed in Chapter~\ref{sec:fn-labels-implicit}, function labels are implicit in TrueType. We first preprocess the fpgm program and get a structured function table. Figure~\ref{fig:preprocess} shows an example of the TrueType bytecode after being preprocessed. The fpgm and endf instructions are completely removed and every function definition starts with its function label. In this way, function definitions become explicit, which also provides a concrete function table for implementing function call in later steps. Internally, the function table is a hash map structure that maps function labels to functions.
\begin{figure}[h] 
\caption{Example of Function Definitions After Preprocessing} 
\label{fig:preprocess} \centering
\begin{lstlisting}
Function #1
 DUP([])
 RCVT([])
 SWAP([])
 DUP([])
 PUSH([205])
 WCVTP([])
 SWAP([])
 DUP([])
 PUSH([346])
 LTEQ([])
 IF([])
    SWAP([])
    DUP([])
    PUSH([141])
    WCVTP([])
    SWAP([])
 EIF([])
 DUP([])
 PUSH([237])
 LTEQ([])
\end{lstlisting}
\end{figure}

Consider a sequence such as \texttt{push[1] call[]}.
When executing the {\tt call}, we store the current program counter into \textbf{back\_pointers}.
Next, we look up the function labelled 1. 
Then, we move the program pointer to the beginning of the function. 
Once we finish executing the last instruction in function 1, we pop the program counter from \textbf{back\_pointers}, returning the counter to the caller. This is analogous to the execution stack in usual programs. 
\item \textbf{If-Else Clause} 
Similarly to how we implement function calls, we also store the original program counter into \textbf{back\_pointers}.
Besides the program pointer itself, we also store the environment context associated with the program pointer.
After we finish traversing one branch, we need to restore the program pointer, as well as the environment context, to traverse the other branch. 

After we traverse all branches, we then merge the branches' environment contexts.
We have added assertions to our interpreter to enforce that the stack height of different branches must always be the same. This property is not required in the TrueType specification. However, we found that in our experiments, stack heights on different branches were indeed always the same.

Although stack heights are the same, stack values may of course differ. In that case, we generate an OR expression to merge together results.

\end{itemize}
\section{Translating TrueType to COI}
Having preprocessed our bytecode and generated abstract values, the next step is
to generate three-address code.

\subsection{Properties of COI}
Our three-address code, COI, has the following properties.
\begin{itemize}
\item COI is stack-less. Unlike TrueType bytecode, COI has completely removed the stack concept from the language definition. 
\item Every local variable is explicit and is initialized before use. 
\item The number of operands of every expression is no more than three. Instruction operands must be variables. 
\end{itemize}

We next divide the TrueType instructions into different categories and 
discuss the translation from TrueType bytecode to equivalent COI instructions for every category. 

\subsection{Assignment Statements}
We introduce the notion of variable in COI, which is absent from the TrueType bytecode, and introduce assignment statements to associate the data on the stack with variable names. In general, the main difficulties in analyzing TrueType bytecode are due to the fact that bytecode instructions affect the program stack, and thus have implicit uses of values on the stack. COI instructions can instead refer to data by variable identifier, eliminating implicit uses of values on the stack. This completely removes the complexities of calculating the actual stack depth from the bytecode analysis phase, making sophisticated analyses possible.
After we have introduced assignment statements and variable names, our analyses can reason about data as identified by variable names, thus removing complications introduced by the stack.
Listings~\ref{lst:asst-tt} and~\ref{lst:asst-coi} show before-and-after views of push instructions in TrueType and as converted to COI, respectively.
 
\noindent\begin{minipage}{.45\textwidth}
\begin{lstlisting}[caption=TrueType,label=lst:asst-tt]{Name}
PUSH[ ]  /* 7 values pushed */
71 70 45 31 16 51 15 85 7
\end{lstlisting}
\end{minipage}
 \hfill\vrule\hfill
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[caption=COI,label=lst:asst-coi]{Name}
prep0:=71
prep1:=70
prep2:=45
prep3:=31
prep4:=16
prep5:=51
prep6:=15
prep7:=85
prep8:=7
\end{lstlisting}
\end{minipage}

\subsection{Arithmetic and Logic Operations}
We designed a group of arithmetic and logic operation statement in COI. It is straightforward to translate
TrueType arithmetic and logic operations (all binary) into their COI equivalents.
COI arithmetic and logic statements all have the same format \textbf{op3 = op1 op op2} where the right-hand
side contains one operation and two operands and the left side has one variable.
Table \ref{table:3} shows the translation from TrueType arithmetic and logic operations to equivalent COI instructions.

\newpage
\begin{longtable}{p{8em}p{10em}}
{\bf TrueType Code} & {\bf COI Code}\\ 
\tt PUSH[]

1 2

ADD[] & \tt  v1 = 1

v2 = 2

v3 = v1 + v2\\[1em]

\tt PUSH[]

1 2

SUB[] & \tt v1 = 1

v2 = 2

v3 = v1 - v2 \\[1em]

\tt PUSH[]

1 2

DIV[] & \tt v1 = 1

v2 = 2

v3 = v1 / v2 \\[1em]

\tt PUSH[]

1 2

MUL[] &\tt   v1 = 1

v2 = 2

v3 = v1 * v2\\[1em]

\tt PUSH[]

1 0

AND[] &\tt  v1 = 1

v2 = 0

v3 = v1 AND v2\\[1em]

\tt PUSH[]

1 0

OR[] & \tt v1 = 1

v2 = 0

v3 = v1 OR v2 \\[1em]

\tt PUSH[]

1 2

EQ[] & \tt 
v1 = 1

v2 = 2

v3 = v1 == v2\\[1em]

\tt PUSH[]

1 2

NEQ[] &

\tt v1 = 1

v2 = 2

v3 = v1 != v2 \\[1em]
\caption{COI Representations of TrueType Arithmetic and Logic Operations} \label{table:3}\\
\end{longtable}
\subsection{IF/ELSE/EIF}
To represent bytecode if-else clauses (which are, fortunately, structured), we designed an if-else clause in COI that explicitly represents conditions and includes pair braces. TrueType includes both if/endif and if/elseif/endif forms. We illustrate the conversion below.
\begin{itemize}
\item
\textbf{TrueType Instruction: IF[]/EIF[]} 

\begin{center}
\begin{tabular}{ll}
  {\bf TrueType}&{\bf COI}\\[1em]
  
\begin{minipage}{10em}
\small \begin{verbatim}
 PUSH([1, 2])
 RS([])
 EQ([])
 IF([])
	PUSH([2, 3])
	ADD[]
 EIF([])
PUSH[2,1]
SUB[]
\end{verbatim}
\end{minipage} & 
\begin{minipage}{10em} \small
\begin{verbatim}
v1 = 1
v2 = 2
v3 = storage_area[v2]
v4 = v3 == v1
if (v4) {
    v5 = 2
    v6 = 3
    v7 = v5 + v6 }
v7 = 2
v8 = 1
v9 = v8 - v7
\end{verbatim}
\end{minipage}
\end{tabular}
\end{center}
\item
\textbf{TrueType Instruction: IF[]/ELSE[]/EIF[]}

\begin{center}
\begin{tabular}{ll}
  {\bf TrueType}&{\bf COI}\\[1em]
\begin{minipage}{10em} \small
\begin{verbatim}
PUSH([1, 2])
 RS([])
 EQ([])
 IF([])
    PUSH([2, 3]
    ADD[]
 ELSE([])
    PUSH([2, 1])
    SUB[]
 EIF([])
 PUSH[3,1]
 DIV[]
\end{verbatim}
\end{minipage} & 
\begin{minipage}{10em}
  \small
\begin{verbatim}
v1 = 1
v2 = 2
v3 = storage_area[v2]
v4 = v1==v3
if (v4) {
    v8 = 2
    v9 = 3
    v10 = v8 + v9 }
else {
    v5 = 2
    v6 = 1
    v7 = v5 - v6 }
v10 = 3
v11 = 1
v12 = v10/v11
\end{verbatim}
\end{minipage}
\end{tabular}
\end{center}
\end{itemize}
\subsection{Function call}
The following is an example of a function call. Note that the target of the call is implicit
in TrueType bytecode.
\begin{verbatim}
PUSH([83])
CALL[]
\end{verbatim}
Assume that the function table contains a function labeled 83.
\begin{verbatim}
Function #83:
PUSH([18])
 SVTCA([0])
 MPPEM([])
 SVTCA([1])
 MPPEM([])
 EQ([])
 WS([])
\end{verbatim}
The function call can then be translated into COI code:
\begin{verbatim}
prep19 = 83
CALL(prep19)
BeginFunctionCALL:
fpgm0:=18
GS[freedom_vector]:=0
GS[projection_vector]:=0
fpgm1:=PPEM_Y
GS[freedom_vector]:=1
GS[projection_vector]:=1
fpgm2:=PPEM_X
fpgm3:=fpgm2 == fpgm1
storage_area[$fpgm0]=$fpgm3
EndFunctionCALL
\end{verbatim}
\subsection{Reads and writes from storage area and CVT table}
These instructions can be straightforwardly translated into pseudo-assignment
instructions.
\begin{itemize}
\item \textbf{RS[] Read Store}

The RS[] can be translated into \textbf{v1 = storage\_area[v2]}.
\item \textbf{WS[] Write Store}

The WS[] can be translated into \textbf{storage\_area[v1] = v2}.
\item \textbf{RCVT[] Read Control Value Table entry}

The RCVT[] can be translated into \textbf{v1 = cvt\_table[v2]}.
\item \textbf{WCVTF[]/WCVTP[] Write Control Value Table in Funits/Pixel units}

The WCVTF[] can be translated into \textbf{cvt\_table[v1] = v2}.

\end{itemize}
\subsection{Instructions that manage the stack}

We use our abstract interpreter to calculate the current stack depth at each instruction in the TrueType program. Using that
information, we can refer to the right variables in the corresponding COI instruction.

Some instructions simply modify the program stack's depth without introducing data or referring to any values from the stack. Examples include CLEAR[] and POP[]. We need not translate such instructions to COI; it suffices to manipulate the abstract stack depth in the interpreter.
\begin{itemize}
\item \textbf{CLEAR[] (CLEAR the stack)}
This instruction clears the all data from the program stack, so that the program stack will be empty afterwards.
We do not translate the CLEAR[] statement. We simply stop referring to the variables
holding data that was cleared, and set the depth of the program variables' stack to 0 in the abstract interpreter.
\item \textbf{DEPTH[] (DEPTH of the stack)}
We get the current depth of the stack from the abstract interpreter, and assign that depth value to a variable.
The DEPTH[] instruction thus gets translated into a variable assignment \textbf{v = SIZE}.
\item \textbf{DUP[] (DUPlicate top stack element)}
To translate the DUP statement, we create a new variable and assign to it the variable 
on top of the stack. 
For example:
\begin{center}
\begin{tabular}{ll}
  {\bf TrueType}&{\bf COI}\\[1em]
\begin{minipage}{10em} 
\begin{verbatim}
PUSH([2])
DUP[]
\end{verbatim}
\end{minipage} & 
\begin{minipage}{10em}
\begin{verbatim}
v1 = 2 
v2 = v1
\end{verbatim}
\end{minipage}
\end{tabular}
\end{center}
\item \textbf{POP[] (POP top stack element)}

We do not translate the POP[] statement. We simply need to decrement the stack pointer in the interpreter state. 
\item \textbf{CINDEX[] (Copy INDEXed element)}

As with DUP[], we also create a new variable and copy the
indexed element in program stack.
For example (the second indexed element in the stack is v1):
\begin{center}
\begin{tabular}{ll}
  {\bf TrueType}&{\bf COI}\\[1em]
\begin{minipage}{10em} 
\begin{verbatim}
PUSH([3,4,2])
CINDEX[]
\end{verbatim}
\end{minipage} & 
\begin{minipage}{10em}
\begin{verbatim}
v1 = 3
v2 = 4 
v3 = 2
v4 = v1
\end{verbatim}
\end{minipage}
\end{tabular}
\end{center}
\item \textbf{MINDEX[] (Move INDEXed element)}
  
  This instruction is similar to {\bf CINDEX[]}.
\begin{center}
\begin{tabular}{ll}
  {\bf TrueType}&{\bf COI}\\[1em]
\begin{minipage}{10em} 
\begin{verbatim}
PUSH([3,4,2])
MINDEX[]
\end{verbatim}
\end{minipage} & 
\begin{minipage}{10em}
\begin{verbatim}
v1 = 3
v2 = 4 
v3 = 2
v4 = v1
\end{verbatim}
\end{minipage}
\end{tabular}
\end{center}
\item \textbf{ROLL[] (ROLL top 3 stack elements)}

 We use variable assignments to transform the ROLL[] instruction.
\begin{center}
\begin{tabular}{ll}
  {\bf TrueType}&{\bf COI}\\[1em]
\begin{minipage}{10em} 
\begin{verbatim}
PUSH([3,4,2])
ROLL[]
\end{verbatim}
\end{minipage} & 
\begin{minipage}{10em}
\begin{verbatim}
v1 = 3
v2 = 4 
v3 = 2
$temp = v3
v3 = v1
v1 = v2
v2 = $temp
\end{verbatim}
\end{minipage}
\end{tabular}
\end{center}
\item \textbf{SWAP[] (SWAP top two stack elements)}
\begin{center}
\begin{tabular}{ll}
  {\bf TrueType}&{\bf COI}\\[1em]
\begin{minipage}{10em} 
\begin{verbatim}
PUSH([3,4])
SWAP[]
\end{verbatim}
\end{minipage} & 
\begin{minipage}{10em}
\begin{verbatim}
v1 = 3
v2 = 4 
$temp = v1
v1 = v2
v2 = $temp
\end{verbatim}
\end{minipage}
\end{tabular}
\end{center}
\end{itemize}
\subsection{Graphics State Related Instructions}
We convert graphics state instructions into pseudo-assignments, and include
two examples.
\begin{itemize}
\item \textbf{SLOOP[] (Set LOOP variable)}

SLOOP[] is translated into \textbf{GS[loop] = v1}.

\item \textbf{SPVTCA[a] (Set Projection Vector To Coordinate Axis)}

  SPVTCA[a] is translated into \textbf{GS[projection\_vector] = a}.
\end{itemize}

We hope that the loop variable remains concrete, since loop-related instructions
use the loop variable to pop values from stack.
For other graphics state, we do not need to keep values.
We retain graphics state-related instructions in COI only to enable us to transform COI back to TrueType bytecode.

\subsection{Other}
Instructions in this category all map into (hard-coded COI) function calls with or without return values. 
This category include two categories of instructions:
\begin{enumerate}
\item Instructions that only pop data from stack and push nothing onto the stack. This kind of instruction only
affects the height of the program stack, and typically has some other (drawing-related) side effect. We map instructions onto function calls without return values. For example, ALIGNPTS[] becomes ALIGNPTS(p1, p2).
\item Instructions that push one return result onto the stack. We map these instructions onto function calls
with a return value. For example, we transform MAX[] to v3 = MAX(v1, v2), and MPPEM[] to v1 = MPPEM().
\end{enumerate}

We transform each instruction concurrently with the abstract interpretation of the instruction.
We push every new declared variable, along with its value, onto the variable stack. During instruction execution, we pop the corresponding variables from the stack.

\section{Possible Optimizations}
Transforming TrueType bytecode into COI allows for standard compiler optimizations.
In this section, we list three example optimizations that could be applied to COI.
Currently, we have implemented constant propagation for COI, and discuss results in chapter~\ref{experiment}.

\subsection{Constant propagation}

Using constant propagation, we gained a complete set of function calls used in each \textit{glyph program} and \textit{control value program}.

\subsection{Dead code elimination}

After obtaining the set of functions used in glyph programs, we could perform dead code elimination the
\textit{font program} and remove the functions that are declared but never used.

\subsection{Branch Elimination}
If we could get all possible environmental input (for a fixed device), we could perform branch elimination and delete branches that will never be executed. Both device-specific and device-independent optimizations are possible. In the device-specific version, we would get a font file optimized for
a certain device.

\section{Experimental Methodology}
We ensured the correctness of our implementation using several approaches.
\begin{itemize}
\item \textbf{Assertions in the Abstract Interpreter}

Some invariants in TrueType bytecode can be translated into 
program assertions: for instance, keys for the CVT table and write storage indexed read are constant. Also, the key indexed value must have been previously written-to. At the end of execution, program stack height
is always zero. There must never be a stack underflow.

We added the above assertions into the implementation of the abstract interpreter, 
ensuring that these invariants always hold during program execution.
\item \textbf{Create test cases for every instruction}

We manually created a suite of test cases to test the result of transforming every TrueType instruction into COI. This helps to verify the correctness of our implementation of the transformation process.
\item \textbf{Manual Inspection}

For each instruction execution, our abstract interpreter's logging will show how the instruction affects the program environment.
We manually read the logging for every instruction to ensure the correct behavior of our abstract interpreter.


\item \textbf{Test Against TrueType Interpreter}
We instrumented the mature TrueType interpreter, FreeType~\cite{FreeType}, to print out target function labels for call sites that it encounters in TrueType bytecode.
We then compared our experimental results in chapter~\ref{experiment} against the results from FreeType.
This method helps ensures the correctness of our experimental results as reported in Chapter \ref{experiment}.
\end{itemize}
