\chapter{Motivation}
We have previously described the TrueType font format, which includes TrueType bytecode. We now continue by discussing the motivation for TrueType bytecode analysis and optimization.
We demonstrate the need for a new intermediate representation for TrueType bytecode by illustrating several difficulties which arise when attempting to optimize the stack-based TrueType bytecode directly.

\section{Why do we need TrueType bytecode analysis and optimization?}

Although each glyph in a font has its own bytecode (under the instructions tag), glyphs also share a global function table, 
defined under the fpgm tag. 
Due to the stack-based nature of TrueType bytecode, current subsetting techniques keep the entire global function table for every font file, producing a considerable amount of unused bytecode in font files. 

As the purpose of font subsetting is to produce a font file which is as small as possible, the lack of TrueType bytecode analysis and optimization techniques harms the effectiveness of font subsetting techniques.

\section{Why we can't transform bytecode directly?}
To answer this question, we discuss the advantages and disadvantages of TrueType bytecode, which is stack-based.
\subsection{Advantages of stack-based languages}
In this section, we discuss the advantages of stack-based languages and speculate about why TrueType bytecode was originally designed as a 
stack-based language. Three advantages:
\begin{enumerate}
\item Instruction encoding is compact. As most operands are implicit, instructions can often be encoded as a single opcode without any operands. The instructions of stack-based virtual machines are called bytecodes mainly because opcodes are often a single byte long.

\item Interpreters for stack-based languages are more straightforward to implement.
This property is very important for font files, which must be rendered on many different kinds of devices, some of which are heavily resource-constrained, and 
such devices require different font engines. The stack-based design of TrueType bytecode simplifies
font engine development across different platforms.
Moreover, the source code for stack-based language interpreters tends to be more compact, readable and more maintainable.

\item Syntax for stack-based languages tend to be less complicated. The specification of TrueType is quite simple
compared to non-stack based languages: for instance, there are no variable names or declarations. All data is pushed onto the
stack and the following operations execute directly on operands popped from the stack.
\end{enumerate}
\subsection{Disadvantages of optimizing TrueType bytecode directly}
Here we list several critical reasons why we cannot optimize TrueType bytecode directly (i.e. without transformation).
\begin{enumerate}
\item Due to the complexities of stack-based languages, changing instruction order can be 
fairly complicated. Related instructions might exist
arbitrarily far away from each other. For example, TrueType programs often push all data at the beginning
of a program, followed by a sequence of statements.
Therefore, there is no straightforward way to reason about which value is used by which statement.
For this reason, identifying dependencies between instructions is quite challenging.
\item The complexities of TrueType bytecode increase with control flow---conditions for if statements and targets for function calls popped from the stack
\item Statements and expressions are not explicit. As all operands are popped from stack, there is no easy way to construct the expressions statically. In addition, expressions can be arbitrary large.
For instance, Figure~\ref{fig:expression} shows code whose result evaluates to the expression v = (PPEM \textgreater 2047) OR (PPEM \textless 9).
\begin{figure}[h]
\caption{TrueType bytecode computing (PPEM \textgreater 2047) OR (PPEM \textless 9)}
\label{fig:expression}
  \centering
\begin{lstlisting}
      MPPEM[ ]
      PUSH[ ]
      2047
      GT[ ]
      MPPEM[ ]
      PUSH[ ]
      9
      LT[ ]
      OR[ ]
\end{lstlisting}
\end{figure}
\item \label{sec:fn-labels-implicit} Function labels are not explicit. The code snippet in Figure~\ref{fig:functiondefinition} defines two functions: Function 1 and Function 2.
FDEF starts a function definition and ENDF ends a function definition.
The instruction \textbf{FDEF} (function define) pops one element from the stack and treats that element as the function label. It is complicated to infer
the value of the top of the stack, and we therefore say that the function label is implicit.
\begin{figure}[h]
\caption{Function labels are implicit in TrueType}
\label{fig:functiondefinition}
\begin{lstlisting}
      PUSH[ ]
      2 1
      FDEF[ ]
      DUP[ ]
      PUSH[ ]
      1
      ADD[ ]
      RCVT[ ]
      PUSH[ ]
      3
      CINDEX[ ]
      DUP[ ]
      SRP1[ ]
      GC[0]
      SUB[ ]
      SWAP[ ]
      RCVT[ ]
      SWAP[ ]
      SUB[ ]
      SCFS[ ]
      ENDF[ ]
      FDEF[ ]
      DUP[ ]
      RCVT[ ]
      RTG[ ]
      ROUND[00]
      WCVTP[ ]
      ENDF[ ]
\end{lstlisting}
\end{figure}

\item Callees are implicit and read off the stack. Furthermore, since the font engine has only one stack, 
a function will apply its actions to the font engine's single operand stack.
As a result, when we encounter a sequence of function calls like the last three lines in Figure~\ref{fig:control}, there
is no straightforward way to understand which three functions are called; the destination of the second CALL
may in principle depend on the effects of the first function. There is no requirement that a callee
leave the operand stack in the same state as upon entry (and indeed, function parameters and return
values may be passed on the stack).

\begin{figure}
\caption{CALL instructions' destinations are implicit in TrueType bytecode}
\label{fig:control}
\begin{lstlisting}
      IF[ ]
      PUSH[ ]
      16
      SCVTCI[ ]
      PUSH[ ]  /* 2 values pushed */
      22 0
      WS[ ]
      EIF[ ]
      CALL[ ]
      CALL[ ]
      CALL[ ]
\end{lstlisting}
\end{figure}
\item Reasoning about data flow is complicated. Some instructions, like \textbf{MPPEM} and \textbf{GETINFO}, will introduce implicit data flow.
The instruction \textbf{MPPEM} depends on the graphics state. If the graphics state's projection vector is set to ``x-axis'',
pushes the current number of pixels per em in x-direction onto the stack. Otherwise, it
pushes the ppem in y-direction.
\item TrueType bytecode includes loop constructs. These instructions will read the loop state variable from the graphics state and execute the specified number of times. Furthermore, loop iterations are not required to leave the stack height invariant.
\end{enumerate}

In conclusion, TrueType bytecode is designed as a stack-based code because stack-based codes are compact. This facilitates the development of different font engines on different platforms.  However, this design makes it very difficult to reason about, or directly apply standard compiler optimizations to, TrueType bytecode. To solve this problem, we will next demonstrate the design of a new three-address code COI and describe the transformation process from TrueType to COI.
