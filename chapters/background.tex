\chapter{Background}
In this section, we survey background material for this thesis, including digitizing letterform design, TrueType Fonts and TrueType bytecode.
\section{Computer Font}
A computer font is a data file that contains 
the shape information for a set of glyphs (also known as characters or symbols). Font engines render glyphs into matrices of dots or pixels. Each glyph can be rendered at many different sizes. 

Generally, there are three broad categories of computer fonts: \textit{bitmap fonts}, \textit{outline fonts} (also called \textit{vector fonts}), and \textit{stroke fonts}. These categories differ in how they represent glyph shapes.

Bitmap fonts store the shape of each glyph using a matrix of pixels at each size. Rendering is straightforward.
The major disadvantage of bitmap fonts is that they are non-scalable. Such fonts must be manually re-drawn at each size.

Outline fonts store shape information using B\'ezier curves\footnote{\url{http://en.wikipedia.org/wiki/B\%C3\%A9zier\_curve}}. They also include drawing instructions and mathematical formul\ae{} to identify control points, which define the outlines for each glyph. Several outline font formats are: Type 1 and Type 3 fonts, TrueType fonts and OpenType fonts.


Stroke fonts define a glyph's outline using the vertices of individual strokes and stroke profiles. Stroke-based fonts are mainly used to describe East Asian character sets and ideograms. For a font developer, editing a stroke-based glyph is less painstaking and less error-prone than editing outline-based glyphs. Stroke-based fonts are heavily used on embedded devices in East Asia due to their space saving properties.

\section{TrueType Fonts}
A TrueType font is described by a set of vectors (and the points contained therein).
At the lowest level, each character (glyph) is described by a list of points on a grid~\cite{TrueType}.
Points are divided into two categories: off-curve points and on-curve points. On-curve
points represent the end of a font outline while off-curve points act as control points. 
For instance, two on-curve points represent a straight line.  If a third off-curve
point is added between these two points, the three points together will represent a parabolic curve.

\subsection{TrueType Bytecode}
TrueType bytecode is a widely-used stack-based bytecode format for providing hints for fonts.
bytecode instructions in font files define a sequence of grid-fitting rules to adjust the font outlines and control the display to produce clear, legible text under different screen resolutions.

\subsection{Glyph Program, Font Program and the Control Value Program}
Every glyph can provide a set of instructions in its \textit{glyph program}.
Instructions associated with an entire font as a whole constitute its \textit{Font Program} (found in its TrueType fppm table) and \textit{Control Value Program} (found in its \text{prep} table).

All the functions in a font file are defined in the \textit{font program} and they can be called by all the \textit{glyph programs}.
The \textit{font program} is executed only once. However, whenever the point size or transformation changes, the \textit{control value program} will be executed by the font engine.
The \textit{glyph program} is executed when a specific glyph is requested.

\subsection{The graphics state}
The graphics state is part of the execution context of TrueType instructions. It includes a list of state 
variables. State variables are typed as booleans, integers, points or vectors.

The state variables are: auto flip, control value cut-in, delta base, delta shift, dual projection vector, freedom
vector, instruct control, loop, minimum distance, projection vector, round state, rp0, rp1, rp2, scan control,
scan control, single\_width\_cut\_in, single\_width\_value, zp0, zp1, zp2.

All variables in graphics state have default values. Variables in the graphics state can be set
and used by TrueType instructions.

Our tool only keeps track of state variables that influence the data on the program stack: loop, projection vector, and freedom
vector.

\subsection{The storage area and CVT table}
The font engine maintains two portions of memory: the \textit{storage area} and \textit{CVT table}.

TrueType bytecode can exchange data between the storage area and the program stack
using specific instructions.

The \textit{CVT table} is defined and initialized in a font file's CVT table. TrueType instructions
can read these values. The \textit{CVT table} is used to store the value of certain font features.

\subsection{Information from the execution environment}
The instruction \textbf{MPPEM[]} puts the size in pixels-per-em for the current glyph onto program stack and the instruction \textbf{MPS[]}
retrieves the size in points-per-em.
The \textbf{GETINFO[]} instruction requests the font engine version number.
This information is statically unknown and depends on the font engine's specific runtime execution environment.

\section{Font Engines}
TrueType font engines rasterize a glyph outline in three steps. First, the engine scales a master outline for the requested glyph. This step renders the font from device independent data to device dependent data, 
 generating points at the appropriate size for the target device. In the second step, the engine executes the instructions embedded in the font data. These instructions move points to appropriate positions for the requested rendering resolution.

 In the third step, the font engine's scan converter takes the font outline data and applies a set of rules to determine which pixels will be part of the glyph image the device is going to display. Finally, devices can display or print out the given font to end users.



\section{Font Subsetting}
An entire font may contain thousands of glyphs. However, rendering any document will only require a small fraction of those glyphs. For example, this thesis uses fewer than 100 glyphs. Embedding an entire font file into a document such as web page will cause inefficient space usage. 
Font Subsetting therefore makes a large font file into 
a smaller one that only includes characters that are actually used in the layout.  
For webpages whose selection of glyphs remain static after initial rendering, font subsetting allows content providers to minimize transmission sizes,
which is very useful for mobile-compatible web pages. 

