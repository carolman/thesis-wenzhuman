\chapter{Related Work}
The related work of this thesis can be divided into four categories: automatic hinting; bytecode manipulation tools; and compiler-related work on handling stack-based code and on symbolic execution.

\section{Automatic Font Hinting}
TrueType hinting is a time-consuming painstaking process, and numerous others have worked on automatic font hinting. We describe three automatic font hinting techniques.

Shamir presented a constraint-based approach for automatic hinting~\cite{Shamir:2003:CAA:636886.636887}. His method is capable of identifying hints inside characters, gathering global font information, and linking the font information to characters. The collected information is then organized into font hints used in many widely used hinting technologies, including TrueType and PostScript. This automatic font hinting approach can provide a solid basis for further font optimization and its generic nature can greatly help font designers.

Zongker et al proposed an approach which automatically hints fonts using examples at typical display sizes and screen resolutions~\cite{Zongker:2000:EHT:344779.344969}. Their automatic hinting method matches the outlines of corresponding glyphs in all fonts, and then translates existing hints for glyphs from the source to their specific target fonts. Their methods can help typographers generate hinting for newly created fonts: typographers can simply carry over existing custom modifications from existing fonts. 

Visual TrueType~\cite{Stamm:1998:VTT:647506.726031} is a tool, developed by Stamm, which helps font designers produce TrueType fonts without resorting to low-level assembly code. It represents the design of TrueType fonts with a specialized glyph data structure which can be visualized and authored graphically. This structure is then automatically inserted into TrueType assembly code. Visual TrueType helps designers develop new fonts without having to understand the arcane details of TrueType ByteCode structure.

Automatic hinting tools greatly simplify the work for font designers. However, such tools cannot guarantee the quality of the generated hint code. The hint code could be very inefficient. Our tool could provide optimizations for the generated TrueType code such as dead code removal. We anticipate our tool serving as the final step in automatic hinting tools to perform pre-release optimizations on generated TrueType bytecode.

\section{TrueType Bytecode Manipulation and Validation Tools}
FontTools/TTX is a tool for font file manipulation. It supports reading and writing of TrueType and OpenType fonts.
It also converts TrueType and OpenType fonts (.ttf) to and from an XML-based file format (.ttx)~\cite{Fonttools}.
The input of our tool is the XML-based files generated
by FontTools.
FontTools is also widely used as for font merging and subsetting. However, its font subsetting and 
merging functionalities do not manipulate the TrueType bytecode in font files.
Our tool takes the first step towards the analysis of TrueType bytecode, which could provide valuable information for TrueType bytecode optimizers, as well as for font subsetting and merging.

\section{Stack-based Bytecode Translation}
AST-based intermediate code has been widely used in compilers and optimization frameworks.
Simple C is an AST-structured intermediate language of C used in the McCAT compiler~\cite{hendren1992simple} project at McGill University. Simple C defines a clear structure for C programs, which facilitates the analysis and optimization for such programs.
Influenced by SIMPLE, GIMPLE and GENERIC are both tree representations for C functions~\cite{merrill2003generic}.
The difference between GIMPLE and GENERIC is that GIMPLE is a three-address representation and each statement is restricted to no more than 3 operands. GIMPLE and GENERIC serve as the middle end in the GNU Compiler Collection\footnote{\url{http://en.wikipedia.org/wiki/GNU\_Compiler\_Collection}}.
Also inspired by Simple, Jimple~\cite{vallee1998jimple} is a three-address intermediate representation designed to simplify analysis of Java bytecode. Jimple is used in Soot~\cite{Vallee-Rai:1999:SJB:781995.782008}, a well-known framework for Java bytecode optimization. 
Jimple shows that a three-address intermediate representation could help solve difficulties with optimizing stack-based bytecode. 
The steps of our framework are heavily inspired by Soot. 
Soot achieves Java bytecode optimization in three steps: first, it translates Java bytecode into
three different intermediate representation: BAF, JIMPLE, and GRIMP. BAF is a typed three address code.
Compared to native Java bytecode, BAF is more suitable for transformation and optimization. 
Second, Soot applies several intra-procedural and whole program optimizations directly to the intermediate representations. Finally, Soot translates the optimized intermediate representations back into Java bytecode.

Similarly, as will be later shown in Figure~{\ref{fig:Overview}}, our design is: first, we transform TrueType bytecode into COI; then, we perform optimizations on COI. In the final step (not implemented yet), we would transform the optimized COI back into TrueType.

Soot greatly facilitates various developments for Java bytecode optimization. For example, Dexpler~\cite{Bartel:2012:DCA:2259051.2259056} leverages Soot to translate Android Dalvik bytecodes to Jimple, enabling future Android bytecode analysis and optimization.

TrueType is a less popular language than either C or Java, and there is no intermediate representation designed specially for TrueType bytecode.
Inspired by the success of the intermediate representations listed above, we decided to design a three-address intermediate representation for TrueType.

\section{Abstract Interpretation and Symbolic Execution}
Abstract interpretation performs a partial execution of a program and gains semantic information about the program's data flow and control flow, without necessarily performing calculations on actual values.
The main difference between abstract interpretation and normal (concrete) program interpretation is that values during the execution process may be abstract (rather than actual data). For example, values could be symbolic formulas over the input symbols. Symbolic execution of programs use symbols that represent arbitrary values; such symbols often occur when representing program inputs~\cite{King:1976:SEP:360248.360252}. Symbolic execution is often used to explore every possible input of a program. Instead, we use abstract interpretation to get the information we need to perform TrueType transformations without doing unnecessary calculations and with unknown inputs. 
